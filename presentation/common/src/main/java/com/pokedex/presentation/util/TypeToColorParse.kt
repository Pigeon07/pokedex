package com.pokedex.presentation.util

import androidx.compose.ui.graphics.Color
import com.pokedex.data.model.entity.PokeType
import com.pokedex.presentation.theme.*
import com.pokedex.ui.theme.*

fun parseTypeToColor(pokeType: PokeType): Color {

    return when (pokeType) {
        PokeType.NORMAL -> TypeNormal
        PokeType.FIRE -> TypeFire
        PokeType.WATER -> TypeWater
        PokeType.ELECTRIC -> TypeElectric
        PokeType.GRASS -> TypeGrass
        PokeType.ICE -> TypeIce
        PokeType.FIGHTING -> TypeFighting
        PokeType.POISON -> TypePoison
        PokeType.GROUND -> TypeGround
        PokeType.FLYING -> TypeFlying
        PokeType.PSYCHIC -> TypePsychic
        PokeType.BUG -> TypeBug
        PokeType.ROCK -> TypeRock
        PokeType.GHOST -> TypeGhost
        PokeType.DRAGON -> TypeDragon
        PokeType.DARK -> TypeDark
        PokeType.STEEL -> TypeSteel
        PokeType.FAIRY -> TypeFairy
        PokeType.SHADOW -> TypeShadow
        PokeType.UNKNOWN -> Color.Transparent
        else -> Color.Transparent
    }
}