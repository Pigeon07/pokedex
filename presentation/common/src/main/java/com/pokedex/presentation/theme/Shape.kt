package com.pokedex.presentation.theme

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Shapes
import androidx.compose.ui.unit.dp

private const val bigCornerRadius = 50
private const val smallCornerRadius = 20

val Shapes = Shapes(
    small = RoundedCornerShape(4.dp),
    medium = RoundedCornerShape(4.dp),
    large = RoundedCornerShape(0.dp),
)

val StatBarLeftShape =  RoundedCornerShape(
    topStartPercent = bigCornerRadius,
    bottomStartPercent = bigCornerRadius,
    bottomEndPercent = smallCornerRadius,
    topEndPercent = smallCornerRadius
)

val StatBarRightShape =  RoundedCornerShape(
    topStartPercent = smallCornerRadius,
    bottomStartPercent = smallCornerRadius,
    bottomEndPercent = bigCornerRadius,
    topEndPercent = bigCornerRadius
)

val StatNameShape =  RoundedCornerShape(
    topStartPercent = smallCornerRadius,
    bottomStartPercent = smallCornerRadius,
    bottomEndPercent = smallCornerRadius,
    topEndPercent = smallCornerRadius
)