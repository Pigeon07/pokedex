package com.pokedex.presentation.pokemonlist

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.graphics.toArgb
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.palette.graphics.Palette
import com.bumptech.glide.load.engine.Resource
import com.caverock.androidsvg.SVG
import com.pokedex.data.model.entity.PokemonListEntry
import com.pokedex.data.repository.IPokemonLocalRepository
import com.pokedex.presentation.theme.PokeballTop
import com.pokedex.data.model.ImageType
import com.pokedex.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import java.io.InputStream
import java.net.URL
import javax.inject.Inject

@HiltViewModel
class PokemonListViewModel @Inject constructor(
) : ViewModel() {

    private val defaultDominantColor = com.pokedex.presentation.theme.PokeballTop.toArgb()
    private var filterPhrase = mutableStateOf("")
    private val _pokemonList = MutableStateFlow<List<com.pokedex.data.model.entity.PokemonListEntry>>(mutableListOf())
    private val _pokemonFilteredList = MutableStateFlow<List<com.pokedex.data.model.entity.PokemonListEntry>>(mutableListOf())

    var pokemonList: StateFlow<List<PokemonListEntry>> = _pokemonList
    var filteredList: StateFlow<List<com.pokedex.data.model.entity.PokemonListEntry>> = _pokemonFilteredList
    var imgType = mutableStateOf(com.pokedex.data.model.ImageType.OFFICIAL)

    init {
        collectPokemonListData()
        downloadPokemonInBatches(20, 0, 50000)
    }

    fun searchPokemonList(newPhrase: String) {
        filterPhrase.value = newPhrase
        updateFilteredPokemonList()
    }

    private fun downloadPokemonInBatches(size: Int, offset: Int, allPokemonCount: Int) {
        if (pokemonList.value.size >= allPokemonCount
            && offset != 0 //skip the first call to get max count from api
        ) {
            updateFilteredPokemonList()
            return
        }
        viewModelScope.launch(Dispatchers.IO)
        {
            val result = repo.getPokemonList(size, offset)
            Log.d(
                this.javaClass.simpleName,
                "PokeApi call requested entries: $size | received: ${result.data!!.results.size} | offset: $offset"
            )
            when (result) {
                is Resource.Success -> {
                    val pokedexEntries = result.data.results.mapIndexed { index, entry ->
                        val number = if (entry.url.endsWith("/")) {
                            entry.url.dropLast(1).takeLastWhile { it.isDigit() }
                        } else {
                            entry.url.takeLastWhile { it.isDigit() }
                        }
                        val imgUrlPng =
                            "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${number}.png"
                        val imgUrlSvg =
                            "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${number}.svg"
                        val imgUrlDefault =
                            "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${number}.png"
                        val dominantColor = try {
                            when (imgType.value) {
                                com.pokedex.data.model.ImageType.OFFICIAL -> {
                                    calculateDominantColor(getBitmapFromUrl(imgUrlPng))
                                }
                                com.pokedex.data.model.ImageType.CARTOON -> {
                                    calculateDominantColor(getBitmapFromUrl(imgUrlSvg))
                                }
                                com.pokedex.data.model.ImageType.GAMES -> {
                                    calculateDominantColor(getBitmapFromUrl(imgUrlDefault))
                                }
                            }
                        } catch (ex: Exception) {
                            defaultDominantColor
                        }
                        com.pokedex.data.model.entity.PokemonListEntry(
                            number.toInt(),
                            entry.name,
                            imgUrlPng,
                            imgUrlSvg,
                            imgUrlDefault,
                            dominantColor
                        )
                    }
                    try {
                        Log.i(this.javaClass.simpleName, "inserting....")
                        localRepo.insertRange(pokedexEntries)
                        Log.i(this.javaClass.simpleName, "inserted: ${pokedexEntries.component1()}")
                    } catch (ex: Exception) {
                        Log.e(this.javaClass.simpleName, ex.message.toString())
                    }
//                    if (pokedexEntries.isEmpty() || pokedexEntries.size < size) {
//                        downloadPokemonInBatches(size = 0, offset = offset + size, result.data.count)
//                    } else {
                    downloadPokemonInBatches(size = size, offset = offset + size, result.data.count)
//                    }
                    updateFilteredPokemonList()
                }
                is Resource.Error -> {
                    TODO()
                }
                is Resource.Loading -> TODO()
            }
        }
    }

    private fun updateFilteredPokemonList() {
        val filterResult = pokemonList.value.filter { it.pokemonName.contains(filterPhrase.value) }
        _pokemonFilteredList.value = filterResult
    }

    private fun collectPokemonListData() {
        viewModelScope.launch(Dispatchers.IO) {
            launch {
                localRepo.getAllPokeListEntries().collect {
                    Log.i(this.javaClass.simpleName, "collecting....")
                    _pokemonList.value = it
                    if (_pokemonFilteredList.value.isEmpty())
                        _pokemonFilteredList.value = it
                }
            }
        }
    }

    private fun calculateDominantColor(bitmap: Bitmap): Int {
        val palette = Palette.from(bitmap).generate()
        return palette.dominantSwatch?.rgb ?: defaultDominantColor
    }

    private fun getBitmapFromUrl(url: String): Bitmap {
        return when (imgType.value) {
            com.pokedex.data.model.ImageType.OFFICIAL -> BitmapFactory.decodeStream(URL(url).content as InputStream)
            com.pokedex.data.model.ImageType.GAMES -> BitmapFactory.decodeStream(URL(url).content as InputStream)
            com.pokedex.data.model.ImageType.CARTOON -> {
                //TODO handle properly svg case - now it breaks as it is impossible to make a bitmap from svg this easily
                val svgPicture = SVG.getFromInputStream(URL(url).content as InputStream)
                val bitmap = Bitmap.createBitmap(svgPicture.renderToPicture())
                bitmap.copy(Bitmap.Config.ARGB_8888, true)
            }
        }
    }

    sealed class UiState {
        class Success(var string: String) : UiState()
        class Error(var string: String) : UiState()
    }

}