package com.pokedex.presentation.pokemonlist

import android.graphics.Typeface
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Paint
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.graphics.nativeCanvas
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.ImageLoader
import coil.decode.SvgDecoder
import coil.request.ImageRequest
import com.pokedex.R
import com.pokedex.data.model.entity.PokemonListEntry
import com.pokedex.data.model.ImageType
import com.google.accompanist.coil.LocalImageLoader
import com.google.accompanist.coil.rememberCoilPainter


@Composable
fun PokemonListEntry(
    entry: com.pokedex.data.model.entity.PokemonListEntry,
    navController: NavController,
    modifier: Modifier = Modifier,
    viewModel: PokemonListViewModel = hiltViewModel()
) {
    val defaultBackgroundColor = MaterialTheme.colors.surface
    val resources = LocalContext.current.resources
    val imgType by remember {
        mutableStateOf(viewModel.imgType)
    }
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier
            .shadow(5.dp, RoundedCornerShape(10.dp))
            .clip(RoundedCornerShape(10.dp))
            .aspectRatio(1f)
            .background(
                Brush.verticalGradient(
                    listOf(
                        entry.dominantColor?.let { Color(it) } ?: defaultBackgroundColor,
                        defaultBackgroundColor
                    )
                )
            )
            .clickable {
                navController.navigate(
                    "pokemon_detail_screen/${entry.dominantColor}/${entry.pokemonName}"
                )
            }
    ) {
        Column(
            modifier = modifier
                .padding(8.dp)
                .align(Center)
        ) {
            PokemonEntryImage(
                entry = entry,
                imgType = imgType.value,
                modifier = Modifier
                    .align(CenterHorizontally)
            )
            PokemonNameText(
                name = entry.pokemonName,
                modifier = modifier,
                font = resources.getFont(R.font.pokemon_solid)
            )
        }
    }
}

@Composable
fun PokemonEntryImage(
    entry: com.pokedex.data.model.entity.PokemonListEntry,
    imgType: com.pokedex.data.model.ImageType,
    modifier: Modifier = Modifier
) {
    val imageLoader = ImageLoader.Builder(LocalContext.current)
        .componentRegistry {
            add(SvgDecoder(LocalContext.current))
        }
        .build()

    when (imgType) {
        com.pokedex.data.model.ImageType.OFFICIAL -> {
            Image(
                painter = rememberCoilPainter(
                    request = ImageRequest.Builder(LocalContext.current)
                        .placeholder(R.drawable.pokeball)
                        .data(entry.imageUrlPng)
                        .allowConversionToBitmap(true)
                        .build()
                ),
                contentDescription = entry.pokemonName,
                modifier = modifier
                    .size(120.dp)
            )
        }
        com.pokedex.data.model.ImageType.GAMES -> {
            Image(
                painter = rememberCoilPainter(
                    request = ImageRequest.Builder(LocalContext.current)
                        .placeholder(R.drawable.pokeball)
                        .data(entry.imageUrlDefault)
                        .allowConversionToBitmap(true)
                        .build()
                ),
                contentDescription = entry.pokemonName,
                modifier = Modifier
                    .size(120.dp)
            )
        }
        com.pokedex.data.model.ImageType.CARTOON -> {
            CompositionLocalProvider(LocalImageLoader provides imageLoader) {
                val painter =
                    rememberCoilPainter(
                        request = ImageRequest.Builder(LocalContext.current)
                            .placeholder(R.drawable.pokeball)
                            .data(entry.imageUrlSvg)
                            .build()
                    )
                Image(
                    painter = painter,
                    contentDescription = entry.pokemonName,
                    modifier = Modifier
                        .size(120.dp),
                    alignment = Alignment.Center
                )
            }
        }
    }
}

@Composable
fun PokemonNameText(
    name: String,
    modifier: Modifier = Modifier,
    font: Typeface
) {
    Box(
        contentAlignment = Center,
        modifier = modifier
            .fillMaxWidth()
    ) {
        Canvas(
            modifier = modifier
        )
        {
            val textPaintStroke = Paint().asFrameworkPaint().apply {
                isAntiAlias = true
                style = android.graphics.Paint.Style.STROKE
                textSize = 36f
                typeface = font
                color = android.graphics.Color.BLUE
                strokeWidth = 8f
                strokeMiter = 5f
                strokeJoin = android.graphics.Paint.Join.ROUND
                textAlign = android.graphics.Paint.Align.CENTER
            }

            val textPaint = Paint().asFrameworkPaint().apply {
                isAntiAlias = true
                style = android.graphics.Paint.Style.FILL
                typeface = font
                textSize = 36f
                color = android.graphics.Color.YELLOW
                textAlign = android.graphics.Paint.Align.CENTER
            }
            drawIntoCanvas {
                it.nativeCanvas.clipBounds
                val x = 0f
                var y = 0f
                if (name.length >= 15 && name.contains('-')) {
                    val splitIndex = name.indexOfFirst { char -> char == '-' }
                    val firstLine = name.substring(0, splitIndex)
                    val secondLine = name.substring(splitIndex + 1, name.length)
                    it.nativeCanvas.drawText(firstLine, x, y - 40f, textPaintStroke)
                    it.nativeCanvas.drawText(firstLine, x, y - 40f, textPaint)
                    it.nativeCanvas.drawText(secondLine, x, y - 5f, textPaintStroke)
                    it.nativeCanvas.drawText(secondLine, x, y - 5f, textPaint)
                } else {
                    it.nativeCanvas.drawText(name, x, y, textPaintStroke)
                    it.nativeCanvas.drawText(name, x, y, textPaint)
                }
            }
        }
    }
}