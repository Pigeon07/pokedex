package com.pokedex.presentation.pokemonlist

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.pokedex.data.model.entity.PokemonListEntry
import kotlin.coroutines.EmptyCoroutineContext

@Composable
fun PokemonListScreen(
    navController: NavController,
    viewModel: PokemonListViewModel = hiltViewModel()
) {
    Surface(
        color = MaterialTheme.colors.background,
        modifier = Modifier.fillMaxSize()
    ) {
        Column {
            Spacer(modifier = Modifier.height(20.dp))
//            Image(
//                painter = painterResource(id = R.drawable.pokemon_logo),
//                contentDescription = "Pokemon",
//                modifier = Modifier
//                    .fillMaxWidth()
//                    .align(CenterHorizontally)
//            )
            SearchBar(
                hint = "Search Pokémon...",
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
            ) {
                viewModel.searchPokemonList(it)
            }
            Spacer(modifier = Modifier.height(16.dp))
            PokemonList(navController = navController)
        }
    }
}

@Composable
fun PokemonList(
    navController: NavController,
    viewModel: PokemonListViewModel = hiltViewModel()
) {
    val pokemonList = viewModel.filteredList.collectAsState(EmptyCoroutineContext)

    LazyColumn(contentPadding = PaddingValues(16.dp))
    {
        val itemCount = if (pokemonList.value.size % 3 == 0) {
            pokemonList.value.size / 3
        } else {
            pokemonList.value.size / 3 + 1
        }
        items(itemCount)
        {
            PokedexRow(rowIndex = it, entries = pokemonList.value, navController = navController)
        }
    }
}

@Composable
fun PokedexRow(
    rowIndex: Int,
    entries: List<com.pokedex.data.model.entity.PokemonListEntry>,
    navController: NavController
) {
    Column() {
        Row() {
            PokemonListEntry(
                entry = entries[rowIndex * 3],
                navController = navController,
                modifier = Modifier.weight(1f)
            )
            Spacer(modifier = Modifier.width(8.dp))
            if (entries.size >= rowIndex * 3 + 2) {
                PokemonListEntry(
                    entry = entries[rowIndex * 3 + 1],
                    navController = navController,
                    modifier = Modifier.weight(1f)
                )
                Spacer(modifier = Modifier.width(8.dp))
            } else {
                Spacer(Modifier.weight(1f))
            }
            if (entries.size >= rowIndex * 3 + 3) {
                PokemonListEntry(
                    entry = entries[rowIndex * 3 + 2],
                    navController = navController,
                    modifier = Modifier.weight(1f)
                )
            } else {
                Spacer(Modifier.weight(1f))
            }
        }
        Spacer(modifier = Modifier.height(8.dp))
    }
}