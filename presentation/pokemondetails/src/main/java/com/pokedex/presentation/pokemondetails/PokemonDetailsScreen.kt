package com.pokedex.presentation.pokemondetails

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.produceState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.pokedex.data.model.entity.PokeType
import com.pokedex.data.model.entity.PokemonEntity
import com.pokedex.presentation.pokemondetails.params.PokemonParamsSection
import com.pokedex.presentation.pokemondetails.stats.PokemonStatsSection
import com.pokedex.util.Resource
import com.pokedex.presentation.util.parseTypeToColor
import java.util.*

@Composable
fun PokemonDetailsScreen(
    dominantColor: Color,
    pokemonName: String,
    topPadding: Dp = 20.dp,
    imgSize: Dp = 200.dp,
    navController: NavController,
    viewModel: PokemonDetailsViewModel = hiltViewModel()
) {
    val pokemonInfo = produceState<Resource<com.pokedex.data.model.entity.PokemonEntity>>(initialValue = Resource.Loading()) {
        value = viewModel.getPokemonInfo(pokemonName = pokemonName)
    }
    val imageUrl = produceState(initialValue = "") {
        value = viewModel.getImageUrl(pokemonName = pokemonName)
    }

    val evolutionInfo = produceState<Resource<Map<Int, List<String>>>>(initialValue = Resource.Loading()) {
        value = viewModel.getEvolutionSteps(pokemonName = pokemonName)
    }

    Box(
        contentAlignment = Alignment.TopStart,
        modifier = Modifier
            .fillMaxSize()
            .background(dominantColor)
            .padding(bottom = 16.dp)
    ) {
        PokemonDetailsTopSection(
            navController = navController,
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(0.2f)
                .align(Alignment.TopStart)
        )
        PokemonDetailsWrapper(
            pokemonInfo = pokemonInfo.value,
            modifier = Modifier
                .fillMaxSize()
                .padding(
                    top = topPadding + imgSize / 2f,
                    start = 16.dp,
                    end = 16.dp,
                    bottom = 16.dp
                )
                .shadow(10.dp, RoundedCornerShape(10.dp))
                .clip(RoundedCornerShape(10.dp))
                .background(MaterialTheme.colors.surface)
                .align(Alignment.BottomCenter),
            loadingModifier = Modifier
                .size(100.dp)
                .align(Alignment.Center)
                .padding(
                    top = topPadding + imgSize / 2f,
                    start = 16.dp,
                    end = 16.dp,
                    bottom = 16.dp
                )
        )
        PokemonImage(imageUrl.value, imgSize = imgSize, topPadding = topPadding)
    }
}

@Composable
fun PokemonDetailsTopSection(
    navController: NavController,
    modifier: Modifier = Modifier
) {
    Box(
        contentAlignment = Alignment.TopStart,
        modifier = modifier.background(
            Brush.verticalGradient(
                listOf(
                    Color.Black,
                    Color.Transparent
                )
            )
        )
    ) {
        Icon(
            imageVector = Icons.Default.ArrowBack,
            contentDescription = null,
            tint = Color.White,
            modifier = Modifier
                .size(36.dp)
                .offset(16.dp, 16.dp)
                .clickable {
                    navController.popBackStack()
                }
        )
    }
}

@Composable
fun PokemonDetailsWrapper(
    pokemonInfo: Resource<com.pokedex.data.model.entity.PokemonEntity>,
    modifier: Modifier = Modifier,
    loadingModifier: Modifier = Modifier
) {
    when (pokemonInfo) {
        is Resource.Success -> {
            pokemonInfo.data?.let {
                PokemonDetailsSection(it, modifier)
            }
        }
        is Resource.Error -> {
            Text(
                text = pokemonInfo.message!!,
                color = Color.Red,
                modifier = modifier
            )
        }
        is Resource.Loading -> {
            CircularProgressIndicator(
                color = MaterialTheme.colors.primary,
                modifier = loadingModifier
            )
        }
    }
}

@Composable
fun PokemonDetailsSection(
    pokemonInfo: com.pokedex.data.model.entity.PokemonEntity,
    modifier: Modifier = Modifier
) {
    val scrollState = rememberScrollState()
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier
            .fillMaxSize()
            .offset(y = 24.dp)
            .verticalScroll(scrollState)
    ) {
        PokemonParamsSection(pokemonInfo.weight, pokemonInfo.height)
        Text(
            text = "#${pokemonInfo.id} ${pokemonInfo.name.capitalized()}",
            fontWeight = FontWeight.Bold,
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.h5,
            color = MaterialTheme.colors.onSurface,
        )
        PokemonTypeSection(pokemonInfo.pokeType)

        Text(text = "Base stats:", fontStyle = FontStyle.Italic,color = MaterialTheme.colors.onSurface)
        PokemonStatsSection(pokemonInfo)

        Text(text = "Evolutions:", fontStyle = FontStyle.Italic, color = MaterialTheme.colors.onSurface)

    }
}

@Composable
fun PokemonTypeSection(pokeTypes: List<com.pokedex.data.model.entity.PokeType>) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.padding(16.dp)
    ) {
        for (type in pokeTypes) {
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier
                    .weight(1f)
                    .padding(horizontal = 20.dp)
                    .clip(CircleShape)
                    .background(parseTypeToColor(type))
                    .height(25.dp)
            ) {
                Text(text = type.name, color = Color.White)
            }
        }
    }
}

@Preview
@Composable
fun PokemonDetailsPreview(

) {
    val topPadding: Dp = 20.dp
    val imgSize: Dp = 200.dp
    val navController = rememberNavController()
    val pokemonInfo = Resource.Success<com.pokedex.data.model.entity.PokemonEntity>(
        com.pokedex.data.model.entity.PokemonEntity(
            id = 1,
            name = "pikasaur",
            weight = 10f,
            height = 0f,
            hp = 50,
            attack = 200,
            specialAttack = 15,
            defense = 89,
            specialDefense = 178,
            speed = 110,
            pokeType = listOf(
                com.pokedex.data.model.entity.PokeType.GRASS,
                com.pokedex.data.model.entity.PokeType.ELECTRIC
            ),
            speciesUrl = ""
        )
    )
    val dominantColor = Color.Yellow

    Box(
        contentAlignment = Alignment.TopStart,
        modifier = Modifier
            .fillMaxSize()
            .background(dominantColor)
            .padding(bottom = 16.dp)
    ) {
        PokemonDetailsTopSection(
            navController = navController,
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(0.2f)
                .align(Alignment.TopStart)
        )
        PokemonDetailsWrapper(
            pokemonInfo = pokemonInfo,
            modifier = Modifier
                .fillMaxSize()
                .padding(
                    top = topPadding + imgSize / 2f,
                    start = 16.dp,
                    end = 16.dp,
                    bottom = 16.dp
                )
                .shadow(10.dp, RoundedCornerShape(10.dp))
                .clip(RoundedCornerShape(10.dp))
                .background(MaterialTheme.colors.surface)
                .align(Alignment.BottomCenter),
            loadingModifier = Modifier
                .size(100.dp)
                .align(Alignment.Center)
                .padding(
                    top = topPadding + imgSize / 2f,
                    start = 16.dp,
                    end = 16.dp,
                    bottom = 16.dp
                )
        )
        PokemonImage("abc", imgSize = imgSize, topPadding = topPadding)
    }
}

fun String.capitalized(): String {
    return this.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.ROOT) else it.toString() }
}