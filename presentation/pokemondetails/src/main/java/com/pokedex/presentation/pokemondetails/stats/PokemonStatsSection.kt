package com.pokedex.presentation.pokemondetails.stats

import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.pokedex.data.model.entity.PokemonEntity
import com.pokedex.presentation.theme.*
import com.pokedex.ui.theme.*

@Composable
fun PokemonStatsSection(
    pokemonInfo: com.pokedex.data.model.entity.PokemonEntity
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
    ) {
        PokemonStatItem(
            valueLeft = pokemonInfo.hp,
            valueRight = pokemonInfo.hp,
            name = "HP",
            statColor = HPColor,
            animationDelay = 0
        )
        Spacer(modifier = Modifier.height(4.dp))
        PokemonStatItem(
            valueLeft = pokemonInfo.attack,
            valueRight = pokemonInfo.attack,
            name = "ATK",
            statColor = AtkColor,
            animationDelay = 100
        )
        Spacer(modifier = Modifier.height(4.dp))
        PokemonStatItem(
            valueLeft = pokemonInfo.defense,
            valueRight = pokemonInfo.defense,
            name = "DEF",
            statColor = DefColor,
            animationDelay = 200
        )
        Spacer(modifier = Modifier.height(4.dp))
        PokemonStatItem(
            valueLeft = pokemonInfo.specialAttack,
            valueRight = pokemonInfo.specialAttack,
            name = "SP ATK",
            statColor = SpecialAtkColor,
            animationDelay = 300
        )
        Spacer(modifier = Modifier.height(4.dp))
        PokemonStatItem(
            valueLeft = pokemonInfo.specialDefense,
            valueRight = pokemonInfo.specialDefense,
            name = "SP DEF",
            statColor = SpecialDefColor,
            animationDelay = 400
        )
        Spacer(modifier = Modifier.height(4.dp))
        PokemonStatItem(
            valueLeft = pokemonInfo.speed,
            valueRight = pokemonInfo.speed,
            name = "SPEED",
            statColor = SpeedColor,
            animationDelay = 500
        )
    }
}
