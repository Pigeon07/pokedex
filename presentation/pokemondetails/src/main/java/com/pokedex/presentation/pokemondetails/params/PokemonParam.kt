package com.pokedex.presentation.pokemondetails.params

import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.MonitorWeight
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp


@Composable
fun PokemonParamItem(
    value: Float,
    unit: String,
    icon: ImageVector,
    modifier: Modifier = Modifier
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
        modifier = modifier
            .padding(2.dp)
    ) {
        Icon(
            imageVector = icon,
            contentDescription = null,
            tint = MaterialTheme.colors.onSurface,
            modifier = Modifier
                .weight(2f)
                .size(36.dp)
        )
        Spacer(modifier = Modifier.height(2.dp))
        Text(
            text = "$value$unit",
            color = MaterialTheme.colors.onSurface,
//            fontSize = 8.sp,
            style = MaterialTheme.typography.body1,
            modifier = Modifier
                .weight(1f)
        )
    }
}

@Preview
@Composable
fun PokemonParamItemPreview() {
    PokemonParamItem(
        value = 3.3f,
        unit = "kg",
        icon = Icons.Outlined.MonitorWeight,
        modifier = Modifier.height(80.dp)
        )
}