package com.pokedex.presentation.pokemondetails.evolution

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.pokedex.data.model.entity.EvolutionStepEntity

@Composable
fun EvolutionSection(
    evolutions: List<com.pokedex.data.model.entity.EvolutionStepEntity>,
    modifier: Modifier = Modifier
) {
    val firstForm = evolutions.filter { evo -> evo.evolvingFrom.isNullOrBlank() }
    val steps =
    Row(
        modifier = Modifier.fillMaxHeight()
    ) {
        Column() {

        }
    }
}