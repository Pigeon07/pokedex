package com.pokedex.presentation.pokemondetails.params

import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Height
import androidx.compose.material.icons.outlined.MonitorWeight
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@Composable
fun PokemonParamsSection(
    pokemonWeight: Float,
    pokemonHeight: Float,
    sectionHeight: Dp = 90.dp
) {
    val pokemonWeightInKg = remember {
        pokemonWeight
    }
    val pokemonHeightInMeters = remember {
        pokemonHeight
    }
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .height(sectionHeight)
            .padding(vertical = 8.dp, horizontal = 32.dp)
            .fillMaxWidth()
    ) {
        PokemonParamItem(
            pokemonHeightInMeters,
            "m",
            Icons.Outlined.Height
        )

        PokemonParamItem(
            pokemonWeightInKg,
            "kg",
            Icons.Outlined.MonitorWeight,
        )
    }
}

@Preview
@Composable
fun PokemonParamsSectionPreview() {
    PokemonParamsSection(14.5f, 100.4f)
}
