package com.pokedex.presentation.pokemondetails

import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pokedex.data.repository.EvolutionConverter
import com.pokedex.data.repository.PokemonConverter.mapPokemonEntity
import com.pokedex.data.model.entity.EvolutionStepEntity
import com.pokedex.data.model.entity.PokemonEntity
import com.pokedex.data.model.remote.responses.pokemon.Pokemon
import com.pokedex.data.repository.IPokemonLocalRepository
import com.pokedex.data.model.ImageType
import com.pokedex.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import javax.inject.Inject


@HiltViewModel
class PokemonDetailsViewModel @Inject constructor(
    private val remoteRepo: com.pokedex.data.source.remote.IPokemonRemoteDataSource,
    private val localRepo: com.pokedex.data.repository.IPokemonLocalRepository,
) : ViewModel() {

    var imgType = mutableStateOf(com.pokedex.data.model.ImageType.OFFICIAL)

    suspend fun getPokemonInfo(pokemonName: String): Resource<com.pokedex.data.model.entity.PokemonEntity> {
        val localPokemonEntity = localRepo.getPokemonByName(pokemonName)

        localPokemonEntity?.let {
            return Resource.Success(localPokemonEntity)
        } ?: run {
            var pokemonFromRemote: Resource<Pokemon>
            val result = viewModelScope.async(Dispatchers.IO) {
                pokemonFromRemote = remoteRepo.getPokemonInfo(pokemonName)
                when (pokemonFromRemote) {
                    is Resource.Success -> {
                        val pokemonEntity = mapPokemonEntity(pokemonFromRemote.data!!)
                        localRepo.insertPokemon(pokemonEntity)
                        Resource.Success(pokemonEntity)
                    }
                    else -> Resource.Error("sth went wrong")
                }
            }
            return result.await()
        }
    }

    suspend fun getImageUrl(pokemonName: String): String {
        val result = viewModelScope.async(Dispatchers.IO) {
            localRepo.getImageUrl(imgType = imgType.value, pokemonName = pokemonName)
        }
        return result.await()
    }

    suspend fun getEvolutionSteps(pokemonName: String): Resource<Map<Int, List<String>>> {
        //TODO try fetching from local repo

        val result = viewModelScope.async(Dispatchers.IO) {
            when (val species = remoteRepo.getSpecies(pokemonName)) {
                is Resource.Success -> {
                    val chainId = extractIdFromChainUrl(species.data!!.evolution_chain.url)
                    when (val chain = remoteRepo.getEvolutionChain(chainId)) {
                        is Resource.Success -> {
                            val flatEvolutionsList = com.pokedex.data.repository.EvolutionConverter.mapEvolutionChain(chain.data!!)
                            localRepo.insertEvolutionEntities(flatEvolutionsList)
                            Resource.Success(generateEvolutionMap(pokemonName, flatEvolutionsList))
                        }
                        else -> Resource.Error("Failed to load evolution data")
                    }
                }
                is Resource.Error -> Resource.Error("Failed to load evolution data")
                is Resource.Loading -> TODO()
            }
        }
        return result.await()
    }

    private fun generateEvolutionMap(
        mainPokemonName: String,
        evolutionsSteps: List<com.pokedex.data.model.entity.EvolutionStepEntity>
    ): Map<Int, List<String>> {
        Log.d("||evolutionMap", "generating evolution map for pokemon:$mainPokemonName" )
        //TODO probably can be simplified, messed up logic
        val result = mutableMapOf<Int, List<String>>()

        var currentStep = evolutionsSteps.single { it.pokemonName == mainPokemonName }

        //manage all evolution steps before viewed pokemon
        val previousSteps = mutableListOf<String>()
        while (!currentStep.evolvingFrom.isNullOrBlank()) {
            val previousStep = evolutionsSteps.single { it.pokemonName == currentStep.evolvingFrom }
            previousSteps.add(0, previousStep.pokemonName)
            currentStep = previousStep
        }
        currentStep = evolutionsSteps.single { it.pokemonName == mainPokemonName }
        result[previousSteps.size] = listOf(currentStep.pokemonName)

        var level = 0
        previousSteps.forEach {
            result[level] = listOf(it)
            level++
        }

        //manage all evolution steps after viewed pokemon
        if (evolutionsSteps.any { it.evolvingFrom == currentStep.pokemonName }) {
            var stepLevel = previousSteps.size + 1
            do {
                val nextSteps =
                    evolutionsSteps
                        .filter { step -> step.evolvingFrom == currentStep.pokemonName }
                if (nextSteps.isEmpty()) {
                    break
                }
                result[stepLevel] = nextSteps.map { it.pokemonName }
                currentStep = nextSteps.first()
                stepLevel++
            } while (!currentStep.evolvingFrom.isNullOrBlank())
        }
        return result
    }

    private fun extractIdFromChainUrl(evolutionChainUrl: String): Int {
        val result = if (evolutionChainUrl.endsWith("/")) {
            evolutionChainUrl.dropLast(1).takeLastWhile { it.isDigit() }
        } else {
            evolutionChainUrl.takeLastWhile { it.isDigit() }
        }
        return result.toInt()
    }
}