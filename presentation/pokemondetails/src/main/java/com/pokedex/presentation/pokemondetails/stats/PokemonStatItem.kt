package com.pokedex.presentation.pokemondetails.stats

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.pokedex.presentation.theme.HPColor

@Composable
fun PokemonStatItem(
    valueLeft: Int,
    valueRight: Int,
    name: String,
    statColor: Color,
    height: Dp = 25.dp,
    animationDuration: Int = 1000,
    animationDelay: Int = 0
) {
    Row(
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .height(height)
            .fillMaxWidth()
            .clip(CircleShape)
    ) {
        StatBarLeft(
            value = valueLeft,
            statColor = statColor,
            modifier = Modifier
                .weight(2f)
                .height(height),
            animationDelay = animationDelay,
            animationDuration = animationDuration
        )

        Spacer(modifier = Modifier.width(2.dp))

        StatNameSquare(
            background = statColor,
            modifier = Modifier
                .weight(1f)
                .fillMaxHeight(),
            name = name
        )

        Spacer(modifier = Modifier.width(2.dp))

        StatBarRight(
            value = valueRight,
            statColor = statColor,
            modifier = Modifier
                .weight(2f)
                .height(height),
            animationDelay = animationDelay,
            animationDuration = animationDuration
        )
    }
}


@Preview
@Composable
fun PokemonStatItemPreview() {
    PokemonStatItem(
        valueLeft = 150,
        valueRight = 40,
        name = "HP",
        statColor = HPColor
    )
}