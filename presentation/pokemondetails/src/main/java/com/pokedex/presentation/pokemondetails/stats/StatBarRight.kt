package com.pokedex.presentation.pokemondetails.stats

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.graphics.blue
import androidx.core.graphics.green
import androidx.core.graphics.red
import com.pokedex.presentation.theme.StatBarRightShape

@Composable
fun StatBarRight(
    value: Int,
    statColor: Color,
    modifier: Modifier = Modifier,
    animationDuration: Int = 1000,
    animationDelay: Int = 0,
) {
    val maxStatValue = 255
    val backStatColor = Color(
        red = statColor.toArgb().red,
        green = statColor.toArgb().green,
        blue = statColor.toArgb().blue,
        alpha = 60
    )
    var animationPlayed by remember {
        mutableStateOf(false)
    }

    val curPercent = animateFloatAsState(
        targetValue = if (animationPlayed) {
            value.toFloat() / maxStatValue.toFloat()
        } else {
            0f
        },
        animationSpec = tween(animationDuration, animationDelay)
    )

    LaunchedEffect(key1 = true)
    {
        animationPlayed = true
    }

    Box(
        contentAlignment = Alignment.CenterStart,
        modifier = modifier
            .clip(com.pokedex.presentation.theme.StatBarRightShape)
            .fillMaxWidth()
            .fillMaxHeight()
            .background(
                backStatColor
            )
    )
    {
        Box(
            modifier = modifier
                .fillMaxWidth(curPercent.value)
                .clip(com.pokedex.presentation.theme.StatBarRightShape)
                .background(color = statColor)
        )
        Text(
            text = (curPercent.value * maxStatValue).toInt().toString(),
            fontWeight = FontWeight.Bold,
            color = MaterialTheme.colors.onSurface,
            modifier = Modifier
                .align(Alignment.CenterEnd)
                .offset(x = (-4).dp)
        )
    }
}



@Preview
@Composable
fun StatBarRightPreview() {
    StatBarRight(value = 150, statColor = Color.Green, modifier = Modifier.height(30.dp))
}
