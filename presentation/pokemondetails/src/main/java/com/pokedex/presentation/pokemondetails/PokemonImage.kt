package com.pokedex.presentation.pokemondetails

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import coil.request.ImageRequest
import com.google.accompanist.coil.rememberCoilPainter

@Composable
fun PokemonImage(
    imgUrl: String?,
    imgSize: Dp,
    topPadding: Dp
) {
    Box(
        contentAlignment = Alignment.TopCenter,
        modifier = Modifier
            .fillMaxSize()
    ) {
        imgUrl?.let {
            if (imgUrl.isNotEmpty()) {
                Image(
                    painter = rememberCoilPainter(
                        request = ImageRequest.Builder(
                            LocalContext.current
                        )
                            .placeholder(R.drawable.pokeball)
                            .data(imgUrl)
                            .allowConversionToBitmap(true)
                            .build(),
                        previewPlaceholder = R.drawable.pokeball
                    ),
                    contentDescription = "pokemonImage",
                    modifier = Modifier
                        .size(imgSize)
                        .offset(y = topPadding)
                )
            }
        }
    }
}

@Preview
@Composable
fun PokemonImagePreview() {
    PokemonImage("abc", 200.dp, 20.dp)
}