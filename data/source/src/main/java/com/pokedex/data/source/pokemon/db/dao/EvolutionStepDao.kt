package com.pokedex.data.source.pokemon.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.pokedex.data.model.evolution.entity.EvolutionStepEntity

@Dao
interface EvolutionStepDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertStepsList(evolutionSteps: List<EvolutionStepEntity>)

}