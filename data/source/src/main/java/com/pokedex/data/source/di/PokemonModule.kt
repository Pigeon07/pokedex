package com.pokedex.data.source.di

import android.content.Context
import androidx.room.Room
import com.pokedex.data.source.pokemon.db.PokemonLocalDb
import com.pokedex.data.source.pokemon.db.converter.Converters
import com.pokedex.data.source.pokemon.db.dao.EvolutionStepDao
import com.pokedex.data.source.pokemon.db.dao.PokemonDao
import com.pokedex.data.source.pokemon.remote.Constants.BASE_API_URL
import com.pokedex.data.source.pokemon.remote.PokeRetrofitApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object PokemonModule {

    @Singleton
    @Provides
    fun providePokemonApi(): PokeRetrofitApi {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_API_URL)
            .build()
            .create(PokeRetrofitApi::class.java)
    }

    @Provides
    @Singleton
    fun providePokemonDao(database: PokemonLocalDb): PokemonDao {
        return database.pokemonDao()
    }

    @Provides
    @Singleton
    fun provideEvolutionDao(database: PokemonLocalDb): EvolutionStepDao {
        return database.evolutionDao()
    }

    @Provides
    @Singleton
    fun providePokemonLocalDb(@ApplicationContext appContext: Context): PokemonLocalDb {
        return Room.databaseBuilder(
            appContext,
            PokemonLocalDb::class.java,
            "PokemonLocalDb"
        )
            .addTypeConverter(Converters())
            .build()
    }
}