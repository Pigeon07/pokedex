package com.pokedex.data.source.pokemon.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.pokedex.data.model.PokemonListEntry
import com.pokedex.data.model.pokemon.entity.PokemonEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface PokemonDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertListEntries(pokemons: List<PokemonListEntry>)

    @Query("SELECT * FROM pokemonListEntry ORDER BY id ASC")
    fun getAll(): Flow<List<PokemonListEntry>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPokemon(pokemon: PokemonEntity)

    @Query("SELECT * FROM pokemon WHERE name = :name")
    suspend fun getPokemonByName(name: String): PokemonEntity?

    @Query("SELECT image_url_default FROM pokemonListEntry WHERE pokemon_name = :name")
    suspend fun getDefaultImageUrl(name: String): String

    @Query("SELECT image_url_png FROM pokemonListEntry WHERE pokemon_name = :name")
    suspend fun getOfficialImageUrl(name: String): String

    @Query("SELECT image_url_svg FROM pokemonListEntry WHERE pokemon_name = :name")
    suspend fun getCartoonImageUrl(name: String): String
}