package com.pokedex.data.source.pokemon.remote

object Constants {
    const val BASE_API_URL = "https://pokeapi.co/api/v2/"
    const val PAGE_SIZE = 30
}