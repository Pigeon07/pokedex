package com.pokedex.data.source.pokemon.db.converter

import com.pokedex.data.model.pokemon.entity.PokeType
import com.pokedex.data.model.pokemon.entity.PokemonEntity
import com.pokedex.data.model.pokemon.response.Pokemon
import java.lang.Math.round

object PokemonConverter {

    fun mapPokemonEntity(pokemon: Pokemon): PokemonEntity {
        val stats = mapStats(pokemon)

        return PokemonEntity(
            id = pokemon.id,
            name = pokemon.name,
            weight = round(pokemon.weight * 100f) / 1000f,
            height = round(pokemon.height * 100f) / 1000f,
            hp = stats.getValue("hp"),
            attack = stats.getValue("attack"),
            defense = stats.getValue("defense"),
            specialAttack = stats.getValue("special-attack"),
            specialDefense = stats.getValue("special-defense"),
            speed = stats.getValue("speed"),
            pokeType = mapTypes(pokemon = pokemon),
            speciesUrl = pokemon.species.url
        )
    }

    private fun mapStats(pokemon: Pokemon): Map<String, Int> {
        val resultMap = mutableMapOf<String, Int>()
        val statsList = pokemon.stats

        for (stat in statsList) {
            resultMap[stat.stat.name] = stat.base_stat
        }
        return resultMap
    }

    private fun mapTypes(pokemon: Pokemon): List<PokeType> {
        val results = mutableListOf<PokeType>()
        for (type in pokemon.types) {
            when (type.type.name) {
                "normal" -> results += PokeType.NORMAL
                "fighting" -> results += PokeType.FIGHTING
                "flying" -> results += PokeType.FLYING
                "poison" -> results += PokeType.POISON
                "ground" -> results += PokeType.GROUND
                "rock" -> results += PokeType.ROCK
                "bug" -> results += PokeType.BUG
                "ghost" -> results += PokeType.GHOST
                "steel" -> results += PokeType.STEEL
                "fire" -> results += PokeType.FIRE
                "water" -> results += PokeType.WATER
                "grass" -> results += PokeType.GRASS
                "electric" -> results += PokeType.ELECTRIC
                "psychic" -> results += PokeType.PSYCHIC
                "ice" -> results += PokeType.ICE
                "dragon" -> results += PokeType.DRAGON
                "dark" -> results += PokeType.DARK
                "fairy" -> results += PokeType.FAIRY
                "shadow" -> results += PokeType.SHADOW
                else -> results += PokeType.UNKNOWN
            }
        }
        return results
    }
}