package com.pokedex.data.source.pokemon.db.converter

import androidx.room.ProvidedTypeConverter
import androidx.room.TypeConverter
import com.pokedex.data.model.pokemon.entity.PokeType

@ProvidedTypeConverter
class Converters {

    @TypeConverter
    fun fromString(value: String): PokeType {
        return PokeType.valueOf(value)
    }

    @TypeConverter
    fun pokeTypeToString(type: PokeType): String {
        return type.name
    }

    @TypeConverter
    fun typesFromString(typesString: String): List<PokeType> {
        val typesList = mutableListOf<PokeType>()
        val typesArray = typesString.trim().split(" ")
        for (type in typesArray) {
            typesList.add(PokeType.valueOf(type))
        }
        return typesList
    }

    @TypeConverter
    fun pokeTypeToString(types: List<PokeType>): String {
        val stringBuilder: StringBuilder = java.lang.StringBuilder()
        for (type in types) {
            stringBuilder.append(type.name)
            stringBuilder.append(" ")
        }
        return stringBuilder.toString()
    }
//
//    @TypeConverter
//    fun namesFromString(namesString: String): List<String> {
//        val namesList = mutableListOf<String>()
//        val namesArray = namesString.trim().split(" ")
//        for (name in namesArray) {
//            namesList.add(name)
//        }
//        return namesList
//    }
//
//    @TypeConverter
//    fun pokeNamesListToString(names: List<String>): String {
//        val stringBuilder: StringBuilder = java.lang.StringBuilder()
//        for (name in names) {
//            stringBuilder.append(name)
//            stringBuilder.append(" ")
//        }
//        return stringBuilder.toString()
//    }
//

}