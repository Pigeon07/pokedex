package com.pokedex.data.source.pokemon.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.pokedex.data.model.evolution.entity.EvolutionStepEntity
import com.pokedex.data.model.pokemon.entity.PokemonEntity
import com.pokedex.data.model.PokemonListEntry
import com.pokedex.data.source.pokemon.db.converter.Converters
import com.pokedex.data.source.pokemon.db.dao.EvolutionStepDao
import com.pokedex.data.source.pokemon.db.dao.PokemonDao

@Database(
    exportSchema = false,
    entities = [
        PokemonListEntry::class,
        PokemonEntity::class,
        EvolutionStepEntity::class],
    version = 1
)
@TypeConverters(Converters::class)
abstract class PokemonLocalDb : RoomDatabase() {
    abstract fun pokemonDao(): PokemonDao
    abstract fun evolutionDao(): EvolutionStepDao
}