package com.pokedex.data.source.pokemon.db

import androidx.annotation.WorkerThread
import com.pokedex.data.model.ImageType
import com.pokedex.data.model.PokemonListEntry
import com.pokedex.data.model.evolution.entity.EvolutionStepEntity
import com.pokedex.data.model.pokemon.entity.PokemonEntity
import com.pokedex.data.source.pokemon.db.dao.EvolutionStepDao
import com.pokedex.data.source.pokemon.db.dao.PokemonDao
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PokemonDbDataSource @Inject constructor(
    private val pokemonDao: PokemonDao,
    private val evolutionDao: EvolutionStepDao
)  {

    @WorkerThread
    suspend fun insertRange(pokemons: List<PokemonListEntry>) {
        pokemonDao.insertListEntries(pokemons = pokemons)
    }

    suspend fun getPokemonByName(name: String): PokemonEntity? {
        return pokemonDao.getPokemonByName(name)
    }

    fun getAllPokeListEntries(): Flow<List<PokemonListEntry>> {
        return pokemonDao.getAll()
    }

    suspend fun insertPokemon(pokemon: PokemonEntity) {
        pokemonDao.insertPokemon(pokemon = pokemon)
    }

    suspend fun getImageUrl(imgType: ImageType, name: String): String {
        return when (imgType) {
            ImageType.OFFICIAL -> pokemonDao.getOfficialImageUrl(name)
            ImageType.CARTOON -> pokemonDao.getCartoonImageUrl(name)
            ImageType.GAMES -> pokemonDao.getDefaultImageUrl(name)
        }
    }

    suspend fun insertEvolutionEntities(entities: List<EvolutionStepEntity>) {
        evolutionDao.insertStepsList(entities)
    }
}