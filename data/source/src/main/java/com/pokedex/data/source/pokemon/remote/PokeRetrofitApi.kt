package com.pokedex.data.source.pokemon.remote

import com.pokedex.data.model.evolution.response.EvolutionChain
import com.pokedex.data.model.pokemon.response.Pokemon
import com.pokedex.data.model.pokemon.response.PokemonList
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PokeRetrofitApi {

    @GET("pokemon")
    suspend fun getPokemonList(
        @Query("limit") limit: Int,
        @Query("offset") offset: Int
    ): PokemonList

    @GET("pokemon/{name}")
    suspend fun getPokemonInfo(
        @Path("name") name: String
    ): Pokemon

    @GET("evolution-chain/{id}")
    suspend fun getEvolutionChain(
        @Path("id") id: Int
    ): EvolutionChain

    @GET("pokemon-species/{name}")
    suspend fun getPokemonSpecies(
        @Path("name") id: String
    ): com.pokedex.data.model.species.PokemonSpecies
}