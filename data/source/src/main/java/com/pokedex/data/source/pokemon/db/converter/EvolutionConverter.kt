package com.pokedex.data.source.pokemon.db.converter

import com.pokedex.data.model.evolution.entity.EvolutionStepEntity
import com.pokedex.data.model.evolution.response.EvolutionChain
import com.pokedex.data.model.evolution.response.EvolvesTo

object EvolutionConverter {

    fun mapEvolutionChain(chain: EvolutionChain): List<EvolutionStepEntity> {
        val flatList = mutableListOf<EvolutionStepEntity>()
        val name = chain.chain.species.name
        val isBaby = chain.chain.is_baby
        val evolutionStep = EvolutionStepEntity(
            pokemonName = name,
            isBaby = isBaby,
            evolvingFrom = null
        )
        flatList.add(evolutionStep)
        val nextEvolutionList = chain.chain.evolves_to

        mapChainItemRecursive(
            nextEvolutions = nextEvolutionList,
            list = flatList,
            evolvesFrom = name
        )
        return flatList
    }

    private fun mapChainItemRecursive(
        nextEvolutions: List<EvolvesTo>,
        list: MutableList<EvolutionStepEntity>,
        evolvesFrom: String? = null
    ) {
        if (nextEvolutions.isNullOrEmpty()) {
            return
        }
        for (evolution in nextEvolutions) {
            list.add(
                EvolutionStepEntity(
                    pokemonName = evolution.species.name,
                    evolvingFrom = evolvesFrom,
                    isBaby = evolution.is_baby
                )
            )
            mapChainItemRecursive(
                nextEvolutions = evolution.evolves_to,
                list = list,
                evolvesFrom = evolution.species.name
            )
        }
    }
}