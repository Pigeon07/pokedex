package com.pokedex.data.model.pokemon.response

data class Item(
    val name: String,
    val url: String
)