package com.pokedex.data.model.species

data class Version(
    val name: String,
    val url: String
)