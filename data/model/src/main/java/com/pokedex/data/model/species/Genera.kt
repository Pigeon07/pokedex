package com.pokedex.data.model.species

data class Genera(
    val genus: String,
    val language: com.pokedex.data.model.species.LanguageX
)