package com.pokedex.data.model.species

data class Habitat(
    val name: String,
    val url: String
)