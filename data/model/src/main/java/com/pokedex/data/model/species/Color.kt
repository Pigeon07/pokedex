package com.pokedex.data.model.species

data class Color(
    val name: String,
    val url: String
)