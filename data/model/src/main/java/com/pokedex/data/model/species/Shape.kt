package com.pokedex.data.model.species

data class Shape(
    val name: String,
    val url: String
)