package com.pokedex.data.model.pokemon.response

data class VersionX(
    val name: String,
    val url: String
)