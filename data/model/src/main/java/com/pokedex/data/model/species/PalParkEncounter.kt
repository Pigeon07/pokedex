package com.pokedex.data.model.species

data class PalParkEncounter(
    val area: com.pokedex.data.model.species.Area,
    val base_score: Int,
    val rate: Int
)