package com.pokedex.data.model

enum class ImageType {
    CARTOON,    //SVG
    OFFICIAL,   //PNG
    GAMES       //PNG
}