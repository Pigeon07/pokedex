package com.pokedex.data.model.pokemon.response

data class VersionDetail(
    val rarity: Int,
    val version: VersionX
)