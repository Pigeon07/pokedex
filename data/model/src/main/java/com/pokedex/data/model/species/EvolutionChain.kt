package com.pokedex.data.model.species

data class EvolutionChain(
    val url: String
)