package com.pokedex.data.model.pokemon.response

data class Stat(
    val base_stat: Int,
    val effort: Int,
    val stat: StatX
)