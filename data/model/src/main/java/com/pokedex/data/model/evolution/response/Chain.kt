package com.pokedex.data.model.evolution.response

data class Chain(
    val evolution_details: Any,
    val evolves_to: List<EvolvesTo>,
    val is_baby: Boolean,
    val species: SpeciesX
)