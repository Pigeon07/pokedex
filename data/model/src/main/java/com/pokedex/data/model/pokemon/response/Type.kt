package com.pokedex.data.model.pokemon.response

data class Type(
    val slot: Int,
    val type: TypeX
)