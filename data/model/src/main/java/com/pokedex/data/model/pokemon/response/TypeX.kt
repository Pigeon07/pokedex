package com.pokedex.data.model.pokemon.response

data class TypeX(
    val name: String,
    val url: String
)