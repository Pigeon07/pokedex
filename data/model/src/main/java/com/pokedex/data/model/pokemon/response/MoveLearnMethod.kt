package com.pokedex.data.model.pokemon.response

data class MoveLearnMethod(
    val name: String,
    val url: String
)