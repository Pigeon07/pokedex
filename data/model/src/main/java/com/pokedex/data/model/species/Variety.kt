package com.pokedex.data.model.species

data class Variety(
    val is_default: Boolean,
    val pokemon: com.pokedex.data.model.species.Pokemon
)