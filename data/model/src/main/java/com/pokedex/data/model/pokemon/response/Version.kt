package com.pokedex.data.model.pokemon.response

data class Version(
    val name: String,
    val url: String
)