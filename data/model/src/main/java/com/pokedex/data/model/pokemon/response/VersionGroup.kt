package com.pokedex.data.model.pokemon.response

data class VersionGroup(
    val name: String,
    val url: String
)