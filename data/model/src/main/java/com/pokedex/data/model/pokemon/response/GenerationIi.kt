package com.pokedex.data.model.pokemon.response

data class GenerationIi(
    val crystal: Crystal,
    val gold: Gold,
    val silver: Silver
)