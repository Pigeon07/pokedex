package com.pokedex.data.model.species

data class EggGroup(
    val name: String,
    val url: String
)