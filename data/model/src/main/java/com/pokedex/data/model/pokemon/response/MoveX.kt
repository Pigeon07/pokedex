package com.pokedex.data.model.pokemon.response

data class MoveX(
    val name: String,
    val url: String
)