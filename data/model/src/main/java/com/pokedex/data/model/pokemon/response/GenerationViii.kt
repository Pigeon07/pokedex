package com.pokedex.data.model.pokemon.response

data class GenerationViii(
    val icons: IconsX
)