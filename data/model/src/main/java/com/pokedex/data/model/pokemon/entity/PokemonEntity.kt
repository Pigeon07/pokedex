package com.pokedex.data.model.pokemon.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "pokemon")
data class PokemonEntity(

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    val id: Int,

    @ColumnInfo(name = "name")
    val name: String,

    @ColumnInfo(name = "weight")
    val weight: Float,

    @ColumnInfo(name = "height")
    val height: Float,

    @ColumnInfo(name = "hp")
    val hp: Int,

    @ColumnInfo(name = "attack")
    val attack: Int,

    @ColumnInfo(name = "defense")
    val defense: Int,

    @ColumnInfo(name = "sp_attack")
    val specialAttack: Int,

    @ColumnInfo(name = "sp_defense")
    val specialDefense: Int,

    @ColumnInfo(name = "speed")
    val speed: Int,

    @ColumnInfo(name = "type")
    val pokeType: List<PokeType>,

    @ColumnInfo(name = "species_url")
    val speciesUrl: String,

    )