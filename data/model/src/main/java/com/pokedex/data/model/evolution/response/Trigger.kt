package com.pokedex.data.model.evolution.response

data class Trigger(
    val name: String,
    val url: String
)