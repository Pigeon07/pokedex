package com.pokedex.data.model.evolution.response

data class Species(
    val name: String,
    val url: String
)