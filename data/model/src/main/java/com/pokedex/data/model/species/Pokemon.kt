package com.pokedex.data.model.species

data class Pokemon(
    val name: String,
    val url: String
)