package com.pokedex.data.model.evolution.response

data class SpeciesX(
    val name: String,
    val url: String
)