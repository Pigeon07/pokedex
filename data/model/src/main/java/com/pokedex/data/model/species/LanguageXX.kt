package com.pokedex.data.model.species

data class LanguageXX(
    val name: String,
    val url: String
)