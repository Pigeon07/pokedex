package com.pokedex.data.model.species

data class PokedexNumber(
    val entry_number: Int,
    val pokedex: com.pokedex.data.model.species.Pokedex
)