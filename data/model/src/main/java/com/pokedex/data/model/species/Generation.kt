package com.pokedex.data.model.species

data class Generation(
    val name: String,
    val url: String
)