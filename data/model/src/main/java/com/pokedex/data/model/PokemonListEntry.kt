package com.pokedex.data.model

import androidx.annotation.ColorInt
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "pokemonListEntry")
data class PokemonListEntry(

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    val id: Int,

    @ColumnInfo(name = "pokemon_name")
    val pokemonName: String,

    @ColumnInfo(name = "image_url_png")
    val imageUrlPng: String,

    @ColumnInfo(name = "image_url_svg")
    val imageUrlSvg: String,

    @ColumnInfo(name = "image_url_default")
    val imageUrlDefault: String,

    @ColorInt
    @ColumnInfo(name = "dominant_color")
    val dominantColor: Int?
)
