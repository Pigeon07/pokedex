package com.pokedex.data.model.evolution.response

data class EvolutionChain(
    val baby_trigger_item: Any,
    val chain: Chain,
    val id: Int
)