package com.pokedex.data.model.species

data class Pokedex(
    val name: String,
    val url: String
)