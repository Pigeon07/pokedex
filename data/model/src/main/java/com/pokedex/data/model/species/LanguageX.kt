package com.pokedex.data.model.species

data class LanguageX(
    val name: String,
    val url: String
)