package com.pokedex.data.model.pokemon.response

data class Form(
    val name: String,
    val url: String
)