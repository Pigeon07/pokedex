package com.pokedex.data.model.pokemon.response

data class GenerationV(
    val black_white: BlackWhite
)