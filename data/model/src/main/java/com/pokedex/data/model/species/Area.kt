package com.pokedex.data.model.species

data class Area(
    val name: String,
    val url: String
)