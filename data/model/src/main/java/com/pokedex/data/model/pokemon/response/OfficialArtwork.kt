package com.pokedex.data.model.pokemon.response

data class OfficialArtwork(
    val front_default: String
)