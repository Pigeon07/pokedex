package com.pokedex.data.model.pokemon.response

data class StatX(
    val name: String,
    val url: String
)