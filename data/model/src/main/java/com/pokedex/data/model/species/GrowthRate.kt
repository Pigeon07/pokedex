package com.pokedex.data.model.species

data class GrowthRate(
    val name: String,
    val url: String
)