package com.pokedex.data.model.pokemon.response

data class GenerationIv(
    val diamond_pearl: DiamondPearl,
    val heartgold_soulsilver: HeartgoldSoulsilver,
    val platinum: Platinum
)