package com.pokedex.data.model.species

data class FlavorTextEntry(
    val flavor_text: String,
    val language: com.pokedex.data.model.species.Language,
    val version: com.pokedex.data.model.species.Version
)