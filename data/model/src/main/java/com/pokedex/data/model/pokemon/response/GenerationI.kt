package com.pokedex.data.model.pokemon.response

data class GenerationI(
    val red_blue: RedBlue,
    val yellow: Yellow
)