package com.pokedex.data.model.pokemon.response

data class Species(
    val name: String,
    val url: String
)