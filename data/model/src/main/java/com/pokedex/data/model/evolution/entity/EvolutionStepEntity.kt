package com.pokedex.data.model.evolution.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class EvolutionStepEntity(

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "pokemon_name")
    val pokemonName: String,

    @ColumnInfo(name = "baby")
    val isBaby: Boolean,

    @ColumnInfo(name = "evolving_from")
    val evolvingFrom: String?,
//
//    @ColumnInfo(name = "evolving_to")
//    val evolvingTo: List<String>?
)
