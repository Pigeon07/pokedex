package com.pokedex.data.model.pokemon.response

data class AbilityX(
    val name: String,
    val url: String
)