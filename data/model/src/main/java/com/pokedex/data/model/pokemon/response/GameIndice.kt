package com.pokedex.data.model.pokemon.response

data class GameIndice(
    val game_index: Int,
    val version: Version
)