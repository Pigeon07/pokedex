package com.pokedex.data.model.species

data class Name(
    val language: com.pokedex.data.model.species.LanguageXX,
    val name: String
)