package com.pokedex.data.model.species

data class Language(
    val name: String,
    val url: String
)