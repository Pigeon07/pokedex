package com.pokedex.data.model.pokemon.response

data class Emerald(
    val front_default: String,
    val front_shiny: String
)