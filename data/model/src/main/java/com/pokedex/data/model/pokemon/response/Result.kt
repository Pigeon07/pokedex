package com.pokedex.data.model.pokemon.response

data class Result(
    val name: String,
    val url: String
)